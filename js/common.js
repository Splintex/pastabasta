$(document).ready(function() {
// js scrolpane plugin init
	if ($(".js-scroll-pane").length > 0) {
		$('.js-scroll-pane').jScrollPane({
			autoReinitialise: true
		});
	}

	// init variables
	var link = $(".js-link");
	var link_1 = $(".js-link-1");
	var link_2 = $(".js-link-2");

	var nav_1 = $(".js-nav-1");
	var nav_2 = $(".js-nav-2");

	var menu_1 = $(".js-menu-1");
	var menu_2 = $(".js-menu-2");

	var overlay_1 = $(".js-overlay-1");
	var overlay_2 = $(".js-overlay-2");

	var col_1 = $(".js-col-1");
	var col_2 = $(".js-col-2");

	link.addClass("js-inactive");

	function animate_in_right() {
		link_1.removeClass("js-inactive");
		col_2.addClass("is-active");
		overlay_2.addClass("is-active");
		nav_1.delay(500).fadeIn("slow");
		menu_2.delay(500).fadeIn("slow");
		link_2.addClass("js-active").removeClass("js-inactive");
	}
	function animate_out_right() {
		link_1.addClass("js-inactive");
		col_2.removeClass("is-active");
		nav_1.fadeOut("fast");
		menu_2.fadeOut("fast");
		link_2.removeClass("js-active").addClass("js-inactive");
		overlay_2.removeClass("is-active");
	}
	function animate_in_left() {
		link_2.removeClass("js-inactive");
		col_1.addClass("is-active");
		nav_2.delay(500).fadeIn("slow");
		menu_1.delay(500).fadeIn("slow");
		link_1.addClass("js-active").removeClass("js-inactive");
		overlay_1.addClass("is-active");
	}
	function animate_out_left() {
		link_2.addClass("js-inactive");
		col_1.removeClass("is-active");
		nav_2.fadeOut("fast");
		menu_1.fadeOut("fast");
		link_1.removeClass("js-active").addClass("js-inactive");
		overlay_1.removeClass("is-active");
	}

	link_1.on("click", function(){
		// has class inactive
		if ($(this).hasClass("js-inactive")) {
			animate_in_right();
		}	
		else {
			animate_out_right();
		}

		// has class active
		if ($(this).hasClass("js-active")) {
			animate_out_left();
			animate_in_right();
		}	
		reset_tabs();
	});

	link_2.on("click", function(){
		// has class inactive
		if ($(this).hasClass("js-inactive")) {
			animate_in_left();
		}	
		else {
			animate_out_left();
		}
		// has class active
		if ($(this).hasClass("js-active")) {
			animate_out_right();
			animate_in_left();
		}
		reset_tabs();
	});

// it's a simple example, you should use ajax to improve performance of your site
// tabs
	var tab = $(".js-nav a");
	var tab_cont = $(".js-tab-cont");
	var tab_cont_first = $(".js-tab-cont-first");
	function reset_tabs() {
		tab_cont.hide();
		tab_cont_first.show();
	}
	reset_tabs();
	function tabs() {
		tab.on("click", function(){
			$(this).parent().parent().find("a").removeClass("is-active-link");
			$(this).addClass("is-active-link");
			var index = $(this).attr("href");
			tab_cont.hide();
			$(index).fadeIn("slow");
			console.log("click tab");
			return false;
		});
	}
	tabs();

});